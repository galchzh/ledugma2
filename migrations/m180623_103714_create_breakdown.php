<?php

use yii\db\Migration;

/**
 * Class m180623_103714_create_breakdown
 */
class m180623_103714_create_breakdown extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(),
            'level' => $this->integer(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180623_103714_create_breakdown cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180623_103714_create_breakdown cannot be reverted.\n";

        return false;
    }
    */
}
